import React from 'react';
import styles from './styles.scoped.css';

export default function Card({ image, name, description, price }) {
  return (
    <div className={styles.card}>
      <img alt="img" className={styles.image} src={image} />
      <div className={styles.container}>
        <h4>
          <b className={styles.name}>{name}</b>
        </h4>

        <p className={styles.description}>{description}</p>
        <p className={styles.price}>{price}</p>
      </div>
    </div>
  );
}
