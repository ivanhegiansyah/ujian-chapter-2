import React, { useState, useEffect } from 'react';
import defaultData from './product.json';
import Card from '../../components/elements/Card';
import styles from './styles.scoped.css';
import Search from '../../components/elements/Search';

export default function Products() {
  const [data, setData] = useState(defaultData);
  const [searched, setSearched] = useState('');

  useEffect(() => {
    var regex = new RegExp(searched, 'g');
    searched == ''
      ? setData(defaultData)
      : setData(defaultData.filter((i) => i.name.match(regex)));
  }, [searched]);

  return (
    <>
      <Search
        command={(e) => {
          setSearched(e.target.value);
        }}
      />
      <div className={styles.container}>
        {data?.map((i, idx) => (
          <Card
            description={i.description}
            image={i.image}
            key={idx}
            name={i.name}
            price={i.price}
          />
        ))}
      </div>
    </>
  );
}
